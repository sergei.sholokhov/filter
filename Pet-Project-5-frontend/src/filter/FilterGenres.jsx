// import React, { useEffect, useState } from 'react';
// import { Link } from 'react-router-dom';
// import { Grid, Paper } from '@mui/material';
// // import { makeStyles } from '@mui/styles';
// import { useDispatch, useSelector } from 'react-redux';
// import { fetchProducts, filterByGenre } from '../redux/filterSlice';

// function FilterCategories() {

//   const dispatch = useDispatch();
//   // const classes = useStyles();

//   const handleFilter = (genre) => {
//     dispatch(fetchProducts()).then(() => {
//       dispatch(filterByGenre(genre));
//     });
//   };

//   const genres = [
//     'RPG',
//     'souls like',
//     'dark fantasy',
//     'cyberpunk',
//     'Shooter',
//     'open world'];

//   // const genres = new Map()

//   return (
//     <div style={{ width: '1170px', margin: '0 auto' }}>
//       <Grid container spacing={3}>
//         {genres.map((genre) => (
//           <Grid item xs={4} key={genre}>
//             <Link to="/catalogue" style={{ textDecoration: 'none' }}>
//               <Paper
//                 style={{ height: '100px', textAlign: 'center', padding: '20px', cursor: 'pointer' }}
//                 onClick={() => handleFilter(genre)}
//               >
//                 Genre: {genre}
//               </Paper>
//             </Link>
//           </Grid>
//         ))}
//       </Grid>
//     </div>
//   );
// };

// export default FilterCategories;

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Grid, Paper } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../redux/filterSlice';

function FilterCategories() {
  const dispatch = useDispatch();
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  const products = useSelector((state) => state.products);
  
  useEffect(() => {
    console.log('p', products); 
  }, [products]) // что в депенденси? 



  // const handleFilter = (genre) => {
  //   const products = useSelector((state) => state.products); 
  //   const filtered = products.filter((product) =>
  //     product.genres.some((g) => g.toLowerCase().includes(genre.toLowerCase()))
  //   );
  //   setFilteredProducts(filtered);
  // };

  const genres = [
    'RPG',
    'souls like',
    'dark fantasy',
    'cyberpunk',
    'Shooter',
    'open world'
  ];

  return (
    <div style={{ width: '1170px', margin: '0 auto' }}>
      <Grid container spacing={3}>
        {genres.map((genre) => (
          <Grid item xs={4} key={genre}>
            <Link to="/catalogue" style={{ textDecoration: 'none' }}>
              <Paper
                style={{ height: '100px', textAlign: 'center', padding: '20px', cursor: 'pointer' }}
                onClick={() => handleFilter(genre)}
              >
                Genre: {genre}
              </Paper>
            </Link>
          </Grid>
        ))}
      </Grid>
      {/* Display filtered products */}
      {filteredProducts.map((product) => (
        <div key={product.id}>
          <p>{product.name}</p>
          {/* Add more product info here */}
        </div>
      ))}
    </div>
  );
}

export default FilterCategories;
